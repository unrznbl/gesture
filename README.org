* gesture - handwriting recognizer for linux touchscreen devices

** Dependencies (alpine/postmarketos)
sudo apk add alpine-sdk linux-headers

** Installation

Edit Makefile to change PREFIX which defaults to /usr

Edit gesture2tty to adjust touchscreen device path and tty to inject keys into.

Currently only openrc init scripts are supported.

If no arguments are given configuration of screen width, height and minimum chunk size (roughly the size of your finger in pixels)
is determined dynamically via ioctl calls.
(see below if this doesn't give good results when trying gestures on the touchpad)

Examine/edit gesture.conf to understand/change gesture->key code translation.

Then make and install with:

#+BEGIN_EXAMPLE
    sudo make install
#+END_EXAMPLE

Contributions are welcome. Please send patches to craig at unreasonablefarm dot org.

** Default Gestures (work in progress)

The letters a through z:

[[docs/letters.png]]

The numbers 0 through 9:

[[docs/numbers.png]]

** Debugging

If the device path and/or touch dimensions determined by the program aren't correct try this
interactive tool instead.

```
Usage: touchpad-edge-detector 12x34 /dev/input/event0

This tool reads the touchpad events from the kernel and calculates
 the minimum and maximum for the x and y coordinates, respectively.
The first argument is the physical size of the touchpad in mm.
```
