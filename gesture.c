/* gesture - handwriting recognition */

/*
   (C)opyright 2013-2018 by Craig Comstock <craig@unreasonablefarm.org>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/input-event-codes.h>
#include <linux/input.h>
#include <search.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <limits.h>
#include <stdbool.h>

typedef struct
{
  int x;
  int y;
} point;

#define MAX_GS_POINTS 300
#define MAX_OUTPUT_LENGTH 50

// borrowed from evtest.c
#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) (((x)-1/BITS_PER_LONG)+1)
#define OFF(x) ((x)%BITS_PER_LONG)
#define BIT(x) (1UL<<OFF(x))
#define LONG(x) ((x)/BITS_PER_LONG)
#define test_bit(bit, array) ((array[LONG(bit)] >> OFF(bit)) & 1)

typedef struct {
  int minx, maxx, miny, maxy, numPoints;
  point points[MAX_GS_POINTS];
} gesture;

#define INIT_GESTURE(g) gesture g = {.minx = 0, .maxx = 0, .miny = 0, .maxy = 0, .numPoints = 0}

struct ts_event {
  int x;
  int y;
  int type; // 0 = start, 1 = point, 2 = end
} events[300]; //TODO move events definition to where it is used

// state of various keys
int slash, dot, shift, control, escape, alt, caps;


void handleGesture(gesture gs, int screen_width, int screen_height, int minimum_chunk_size, char *output);
void loadGestures();
void loadFile(char *path, char *dest, int max);

static int is_event_path(const struct dirent *dir)
{
  return strncmp("event", dir->d_name, 5) == 0;
}

int get_touch_info(char *event_path, int *width, int *height, int *chunk)
{
  struct dirent **namelist;
  int i, ndev, devnum;
  int small_dimension;

  ndev = scandir("/dev/input", &namelist, is_event_path, alphasort);
  if (ndev <= 0)
  {
    fprintf(stderr, "Unable to list any device event paths in /dev/input/event*\n");
    return 1;
  }

  int fd = -1;
  unsigned int type;
  unsigned long bit[EV_MAX][NBITS(KEY_MAX)];
  int abs[6] = {0};

  if ( event_path[0] != '\0' )
  {
    fd = open(event_path, O_RDONLY);
  }
  else
  {
    for (i = 0; i < ndev; i++)
    {
      snprintf(event_path, PATH_MAX, "%s/%s", "/dev/input", namelist[i]->d_name);
  
      fd = open(event_path, O_RDONLY);
      if (fd < 0)
        continue;
      memset(bit, 0, sizeof(bit));
      ioctl(fd, EVIOCGBIT(0, EV_MAX), bit[0]);
      if (test_bit(EV_ABS, bit[0])) {
        free(namelist[i]);
        break;
      }
      else
      {
        event_path[0] = '\0';
        close(fd);
      }
    }
  }
  if ( event_path[0] == '\0')
  {
    fprintf(stderr, "Unable to find touch device path. Maybe try evtest and provide path with -p option\n");
    return 1;
  }
  ioctl(fd, EVIOCGABS(ABS_X), abs);
  *width = ( *width == 0 ) ? abs[2] : *width; // only assign if not already assigned
  ioctl(fd, EVIOCGABS(ABS_Y), abs);
  *height = ( *height == 0 ) ? abs[2] : *height;
  // TODO handle rotation? :p
  small_dimension = ( *width < *height ) ? *width : *height;
  // TODO find pixel density or screen physical size and make this about 60 millimeters (3 finger widths)
  *chunk = ( *chunk == 0 ) ? ( small_dimension / 6 ) : *chunk;
  // also get ABSDATA :( so one function to find path and return width, height, chunk

  close(fd);
  free(namelist[i]);
}

void print_event(int type, int code, int value)
{
  fprintf(stderr,"event[type=");
  if (type==EV_SYN) fprintf(stderr,"EV_SYN");
  if (type==EV_KEY) fprintf(stderr,"EV_KEY");
  if (type==EV_REL) fprintf(stderr,"EV_REL");
  if (type==EV_ABS) fprintf(stderr,"EV_ABS");
  fprintf(stderr,"(%hhx)", type);
  fprintf(stderr,", code=");
  if (code==BTN_TOUCH) fprintf(stderr,"BTN_TOUCH");
  fprintf(stderr,"(%hhx)", code);
  fprintf(stderr,", value=%d\n", value);
}

void handleTouchEvents(char *path, int screen_width, int screen_height, int minimum_chunk_size)
{
  fprintf(stderr,"handleTouchEvents()\n");

  int tsfd;
  struct input_event ev;
  point pt;
  INIT_GESTURE(gs);
  char output[MAX_OUTPUT_LENGTH] = ""; // output of handleGesture() should never be very large, maybe bad value in gesture.conf could be a problem.
  tsfd = open(path, O_RDONLY);
  if (tsfd == -1) {
    fprintf(stderr,"problem opening touchscreen device.\n");
    return;
  } else {
    fprintf(stderr, "opened touchscreen device %s ok\n", path);
  }

  int rs = 0;
  int type, code, value;
  bool touch_done = false; // e.g. BTN_TOUCH or ABS_PRESSURE value = 0, finger off the screen
  int gi = 0; // gesture index

  loadGestures();

  // init state of special keys
  slash = dot = control = shift = alt = escape = caps = 0;

  while ((rs = read(tsfd, &ev, sizeof(struct input_event))) != -1) {
    type = ev.type;
    code = ev.code;
    value = ev.value;
    print_event(type, code, value);
    if (type == EV_ABS) {
      if (code == ABS_X) {
        pt.x = value;
        if (pt.x > gs.maxx) {
          gs.maxx = pt.x;
        }
        if (pt.x < gs.minx) {
          gs.minx = pt.x;
        }
      }
      if (code == ABS_Y) {
        pt.y = value;
        if (pt.y > gs.maxy) {
          gs.maxy = pt.y;
        }
        if (pt.y < gs.miny) {
          gs.miny = pt.y;
        }
      }
    }
    fprintf(stderr,"current state, pt.x=%d, pt.y=%d\n", pt.x, pt.y);
    if ((code == BTN_TOUCH || code == ABS_PRESSURE) && value == 0)
    {
      fprintf(stderr,"BTN_TOUCH or ABS_PRESSURE and value == 0\n");
      touch_done = true;
    }
    if (type == EV_SYN && touch_done) {
      fprintf(stderr,"touch done %d points\n", gi);
      gs.numPoints = gi;
      touch_done = false; // reset the state to collect new events
      handleGesture(gs, screen_width, screen_height, minimum_chunk_size, output);
      fprintf(stderr,"output is '%s'(%ld), [0]=(%c)(0x%x)\n", output, strlen(output), output[0], output[0]);
      printf("%s", output);
      output[0] = '\0';
      fflush(NULL);
      gi = 0;
      gs.minx = screen_width;
      gs.miny = screen_height;
      gs.numPoints = 0;
      pt.x = pt.y = -1;
      gs.maxx = gs.maxy = 0;
    }
    if (code == 0 && type == 0 && pt.x != -1 && pt.y != -1) {
      fprintf(stderr,"touch point, x=%d, y=%d\n", pt.x, pt.y);
      if (gi >= MAX_GS_POINTS) {
        fprintf(stderr, "too many points in gesture, ignoring");
      } else {
        gs.points[gi].x = pt.x;
        gs.points[gi].y = pt.y;
        gi++;
      }
      pt.x = pt.y = -1;
    }
  }
  perror("read from touchscreen device");

  close(tsfd);
  return;
}

// minimum_chunk_size should be about the size of your finger on the screen
void handleGesture(gesture gs, int screen_width, int screen_height, int minimum_chunk_size, char *output)
{
  char toput[255] = "";
  fprintf(stderr,"handleGesture numPoints=%d minx=%d maxx=%d miny=%d maxy=%d\n",
         gs.numPoints, gs.minx, gs.maxx, gs.miny, gs.maxy);
  fprintf(stderr,"handleGesture dot=%d\n", dot);
  int key_x[25], key_y[25]; // storage for built up x and y portions of gesture key value
  int i, kxi, kyi;
  // sx,sy are factors by which to derive the simple tx,ty values
  int sx, sy;
  // tx,ty - temp x,y are simple form w/ values of 0,1, or 2.
  int tx, ty;
  // x,y relative to bounding box of gesture
  int rx, ry;
  i = 0;
  kxi = kyi = -1;
  sx = (gs.maxx - gs.minx) / 3;
  sy = (gs.maxy - gs.miny) / 3;
  if (sx < minimum_chunk_size)
    sx = minimum_chunk_size;
  if (sy < minimum_chunk_size)
    sy = minimum_chunk_size;
  // keep track of which corners we reach
  int nw, ne, se, sw;
  nw = ne = se = sw = 0;
  fprintf(stderr,"sx=%d sy=%d\n", sx, sy);
  for (; i < gs.numPoints; i++) {
    rx = gs.points[i].x - gs.minx;
    tx = rx / sx;
    ry = gs.points[i].y - gs.miny;
    ty = ry / sy;
    if (tx == 3)
 /* in case the relative value is on the edge must adjust tx/ty back to values in the range 0..2 */
      tx = 2;
    if (ty == 3)
      ty = 2;
    fprintf(stderr,"minx=%d maxx=%d miny=%d maxy=%d\n", gs.minx, gs.maxx, gs.miny, gs.maxy);
    fprintf(stderr,"x=%d y=%d tx=%d ty=%d\n", gs.points[i].x, gs.points[i].y, tx, ty);
    // only record changes in sections
    if (kxi == -1 || key_x[kxi] != tx) {
      key_x[++kxi] = tx;
    }
    if (kyi == -1 || key_y[kyi] != ty) {
      key_y[++kyi] = ty;
    }
    // keep track of whether we are near the edges
    if (tx == 0 && ty == 0)
      nw = 1;
    if (tx == 2 && ty == 0)
      ne = 1;
    if (tx == 2 && ty == 2)
      se = 1;
    if (tx == 0 && ty == 2)
      sw = 1;
  }
  if (kxi == -1)
    key_x[++kxi] = 0;
  if (kyi == -1)
    key_y[++kyi] = 0;
  char tmp[50];
  char key[51];
  sprintf(key, "");

  if (dot) {
    strncat(key, ".", 50);
    dot = 0;
  }
  if (slash) {
    strncat(key, "/", 50);
    slash = 0;
  }

  i = 0;
  for (; i <= kxi; i++) {
    snprintf(tmp, 50, "%d", key_x[i]);
    strncat(key, tmp, 50);
  }
  strncat(key, ":", 50);
  i = 0;
  for (; i <= kyi; i++) {
    snprintf(tmp, 50, "%d", key_y[i]);
    strncat(key, tmp, 50);
  }
  if (strcmp(key, "0:0") == 0 || strcmp(key, ".0:0") == 0) {
    fprintf(stderr,"in 0:0 case, minx=%d maxx=%d miny=%d maxy=%d screen_height=%d screen_width=%d minimum_chunk_size=%d\n", gs.minx, gs.maxx, gs.miny, gs.maxy, screen_height, screen_width, minimum_chunk_size);
    if (gs.maxy > screen_height - 2 * minimum_chunk_size)
      strncat(key, "s", 50);
    if (gs.miny < 2 * minimum_chunk_size)
      strncat(key, "n", 50);
    if (gs.maxx > screen_width - 2 * minimum_chunk_size)
      strncat(key, "e", 50);
    if (gs.minx < 2 * minimum_chunk_size)
      strncat(key, "w", 50);
  }

  fprintf(stderr,"nw %d ne %d sw %d se %d\n", nw, ne, sw, se);
  if (nw || ne || sw || se) {
    strncat(key, "x", 50);
    if (nw)
      strncat(key, "1", 50);
    if (ne)
      strncat(key, "2", 50);
    if (se)
      strncat(key, "3", 50);
    if (sw)
      strncat(key, "4", 50);
  }

  ENTRY e, *ep;
  e.key = key;
  ep = hsearch(e, FIND);
  char *value;
  value = ep ? ep->data : NULL;

  fprintf(stderr,"key=%s, gesture %s -> %s\n", key, ep ? ep->key : "no key", value);
  fprintf(stderr,"&value=%p\n", &value);

  if (value) {
    if (strcmp(value, "enter") == 0) {
      strcpy(toput, "\x0d");
    } else if (strcmp(value, "tab") == 0) {
      strcpy(toput, "\x09");
    } else if (strcmp(value, "backspace") == 0) {
      strcpy(toput, "\x08");
    } else if (strcmp(value, "space") == 0) {
      strcpy(toput, " ");
    } else if (strcmp(value, "dot") == 0) {
      if (dot) {
        strcpy(toput, ".");
      }
      dot = !dot;
    } else if (strcmp(value, "shift") == 0) {
      if (shift && caps) {
        caps = 0, shift = 0;
      } else if (shift && !caps) {
        caps = 1, shift = 0;
      } else if (!shift && caps) {
        shift = 0, caps = 0;
      } else {
        shift = 1;
      }
    } else if (strcmp(value, "control") == 0) {
      control = !control;
    } else {
      strncpy(toput, value, 255);

      if (strlen(value) == 1) {
        if (caps || shift) {
          toput[0] = toput[0] - 32;
        }
        if (shift && !caps) {
          shift = !shift;
        }
        if (control) {
          toput[0] = toput[0] - 96;
          control = !control;
        }
      }
    }
    fprintf(stderr,"output should be '%s'\n", toput);
    strncpy(output, toput, MAX_OUTPUT_LENGTH);
  }
}

void loadGestures() {
  fprintf(stderr, "loadGestures()\n");
  ENTRY e, *ep;
  hdestroy();
  hcreate(300);

  char buffer[8000];
  char *str = buffer;
  loadFile("/var/lib/gesture/gesture.conf", str, 8000);
  int i = 0;
  char *strptr, *line, *key, *value, *file_parser, *line_parser;
  strptr = str;
  for (;;) {
    line = strtok_r(strptr, "\n", &file_parser);
    fprintf(stderr, "strptr %p, file_parser %p\n", strptr, file_parser);
    strptr = NULL;
    if (line == NULL)
      break;
    if (line[0] == '#')
    {
      fprintf(stderr, "comment line: '%s'\n", line);
      continue;
    }
    fprintf(stderr, "line is '%s'\n", line);
    strptr = line;
    key = strtok_r(strptr, " ", &line_parser);
    fprintf(stderr, "key is '%s'\n", key);
    if (key == NULL)
      break;
    strptr = NULL;
    value = strtok_r(strptr, " ", &line_parser);
    if (value == NULL)
      break;
    fprintf(stderr, "value is '%s'\n", value);
    e.key = key;
    e.data = value;
    ep = hsearch(e, ENTER);
    if (ep == NULL) {
      fprintf(stderr, "failed to add %s -> %s\n", key, value);
    } else {
      //			printf("[%d] key=%s value=%s ep->key=%s\n", i,
      // key, value, ep ? ep->key : "no key");
    }
    i++;
  }
}

void loadFile(char *path, char *dest, int max) {
  //	printf("loadFile\n");
  FILE *fp;
  fp = fopen(path, "r");
  if (fp == NULL) {
    fprintf(stderr, "couldn't open gestures file:%s\n", path);
    exit(3);
  }
  int c;
  int i = 0;
  strcpy(dest, "");

  while ((c = getc(fp)) != EOF) {
    if (i > max-1) {
      fprintf(stderr, "buffer not big enough for gestures file.\n");
      exit(4);
    }
    dest[i] = (char)c;
    i++;
  }
  fclose(fp);
}

int main(int argc, char *const *argv)
{
  int rc, c;
  int digit_optind = 0;
  int screen_width, screen_height, minimum_chunk_size = 0;
  char path[PATH_MAX] = "";

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"path", required_argument, 0, 'p'},
      {"width", required_argument, 0, 'W'},
      {"height", required_argument, 0, 'H'},
      {"minimum", required_argument, 0, 'M'},
      {0, 0, 0, 0}};

    c = getopt_long(argc, argv, "p:W:H:M:", long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
    case 'p':
      strcpy(path, optarg);
      fprintf(stderr,"device path is '%s'\n", path);
      break;
    case 'W':
      screen_width = atoi(optarg);
      fprintf(stderr,"screen_width is %d\n", screen_width);
      break;
    case 'H':
      screen_height = atoi(optarg);
      fprintf(stderr,"screen_height is %d\n", screen_height);
      break;
    case 'M':
      minimum_chunk_size = atoi(optarg);
      fprintf(stderr,"minimum chunk size is %d\n", minimum_chunk_size);
      break;
    default:
      fprintf(stderr,"?? getopt returned character code 0%o ??\n", c);
    }
  }

  if (optind < argc) {
    fprintf(stderr,"non-option ARGV-elements: ");
    while (optind < argc)
      fprintf(stderr,"%s ", argv[optind++]);
    fprintf(stderr,"\n");
  }

  rc = get_touch_info(path, &screen_width, &screen_height, &minimum_chunk_size);
  if (rc != 0)
  {
    printf("Unable to find touch screen device path and dimensions, specify as arguments instead.\n");
  }
  fprintf(stderr, "found screen_width %d, screen_height %d, minimum_chunk_size %d\n", screen_width, screen_height, minimum_chunk_size);

  if (screen_width == 0 || screen_height == 0 || minimum_chunk_size == 0)
  {
    printf("Usage: gesture -p <touchscreen input device path> -W <screen width> -H <screen height> -M <minimum chunk size>\n");
    exit(1);
  }

  handleTouchEvents(path, screen_width, screen_height, minimum_chunk_size);

  return 0;
}
