#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <getopt.h>

int main(int argc, char *const *argv) {
  int c;
  int digit_optind = 0;
  char *path;

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"path", required_argument, 0, 'p'},
      {0, 0, 0, 0}};

    c = getopt_long(argc, argv, "p:", long_options, &option_index);
    if (c == -1)
      break;

    switch(c) {
    case 'p':
      path = optarg;
//      fprintf(stderr,"tty path is '%s'\n", path);
      break;
    default:
      ;
//      fprintf(stderr,"getopt returned character code 0%o\n", c);
    }
  }

  if (optind < argc) {
//    fprintf(stderr,"non-option ARGV-elements: ");
    while (optind < argc)
      ;
//      fprintf(stderr,"%s ", argv[optind++]);
//    fprintf(stderr,"\n");
  }

  int in;
  int f;
  char str[2];

  if ((f = open(path, O_RDWR)) == -1) {
    perror("open tty path");
    return 1;
  }
  while ((in = getchar()) != EOF) {
    str[0] = in;
    str[1] = '\0';
    if (ioctl(f, TIOCSTI, &str) == -1) {
      perror("ioctl");
      return 1;
    }
  }
  close(f);

  return 1;
}
