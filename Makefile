.PHONY: install all

all: gesture injectkeys

gesture: gesture.c
	gcc gesture.c -ggdb -o gesture

injectkeys: injectkeys.c
	gcc injectkeys.c -o injectkeys

install: all
	mkdir -p $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/var/lib/gesture
	mkdir -p $(DESTDIR)/etc/init.d
	cp -f gesture $(DESTDIR)/usr/bin
	cp -f gesture.conf $(DESTDIR)/var/lib/gesture/
	cp -f gesture2tty $(DESTDIR)/usr/bin
	cp -f injectkeys $(DESTDIR)/usr/bin
	cp -f gesture.openrc $(DESTDIR)/etc/init.d/gesture

post-install:
	rc-update add gesture default

pre-deinstall:
	rc-update del gesture

clean:
	rm -f gesture injectkeys
